/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Mensajes extends Thread{
        Socket s;
        PrintWriter out;
        BufferedReader in;  
        boolean salir = false;
    
        public Mensajes(){
            try{
                this.s = new Socket("127.0.0.1",1001);

                out =   new PrintWriter(s.getOutputStream(), true);
            
                in = new BufferedReader(
                        new InputStreamReader(s.getInputStream()));
            }catch (IOException ex) {
                Logger.getLogger(Mensajes.class.getName()).log(Level.SEVERE, null, ex);
            } 
            
        }
        
    public void run(){ 
        String sMensaje="";
        try {
            while(!this.salir){                
                sMensaje = in.readLine();
                
                if (sMensaje!=null){
                    ventanas.VentanaCliente.jTextArea1.setText(ventanas.VentanaCliente.jTextArea1.getText()+"\n"+" < "+sMensaje);
                }                
            }                        
        } catch (IOException ex) {
            Logger.getLogger(Mensajes.class.getName()).log(Level.SEVERE, null, ex);
        }                           
    }
        
    public void send(String mensaje) {
        out.println(mensaje);
    }

    public void cerrar() {
        try {
            s.close();
            in.close();
            out.close();
            this.salir = true;
        } catch (IOException ex) {
            Logger.getLogger(Mensajes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

