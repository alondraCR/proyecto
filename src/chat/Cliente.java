/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Cliente {
    
    
    public static void main(String args[]){
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {                   
                new Cliente().run();
            }
        });
    }  
    
    void run(){
        String sMensajeSalida;
        BufferedReader br =
                   new BufferedReader(new InputStreamReader(System.in));
        
        Mensajes m = new Mensajes();
        m.start();
        
        while(true){                               
            try {                
              sMensajeSalida = br.readLine();
                
                if (sMensajeSalida!=null){
                    System.out.print("> ");
                    m.send(sMensajeSalida);
                    
                    if (sMensajeSalida.toLowerCase().startsWith("salir")){
                        m.cerrar();
                        break;
                    }                    
                }
                                
            } catch (IOException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
    }
}
    